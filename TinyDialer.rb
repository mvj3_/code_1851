final Button button = (Button)findViewById(R.id.button_id);
button.setOnClickListener(new Button.OnClickListener() {
    @Override
    public void onClick(View b) {
    //相应操作,监听器
    }
});

输入的电话号码应为数字，但当在EditText里面输入错误信息时应出现相应的提示信息。判断电话号码的有效性可通过使用android.telephony.PhoneNumberUtils包中的isGlobalPhoneNumber()方法。自此，可在res/values/string.xml里加入
<string name="notify_incorrect_phonenumber">您输入的号码有误，请重新输入！</string>